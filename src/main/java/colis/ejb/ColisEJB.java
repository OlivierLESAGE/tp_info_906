package colis.ejb;

import colis.jpa.Colis;
import colis.jpa.Suivi;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.Date;
import java.util.List;

@Stateless
@LocalBean
public class ColisEJB {
    @PersistenceContext
    private EntityManager em;

    public ColisEJB() {

    }

    public Colis addColis(float poids, float valeur, String origine, String destination) {
        Colis c = new Colis(poids, valeur, origine, destination);
        em.persist(c);
        return c;
    }

    public Colis getColis(long id) {
        return em.find(Colis.class, id);
    }

    public List<Colis> findAllColis() {
        TypedQuery<Colis> rq = em.createQuery("SELECT c FROM Colis c", Colis.class);
        return rq.getResultList();
    }

    public Suivi addSuivi(long IdColis ,double latitude, double longitude, String emplacement, String etat){
        Colis c = this.getColis(IdColis);
        if (c != null) {
            Suivi s = new Suivi(c, latitude , longitude , new Date() , emplacement ,  etat);
            c.addSuivi(s);
            em.persist(s);
            return s;
        } else {
            return null;
        }
    }

    public Suivi getSuivi(long id) {
        return em.find(Suivi.class, id);
    }

    public Suivi updateSuivi(long id ,double latitude, double longitude, String emplacement, String etat){
        Suivi s = this.getSuivi(id);
        
        s.setLatitude(latitude);
        s.setLongitude(longitude);
        s.setEmplacement(emplacement);
        s.setEtat(etat);

        em.merge(s);

        return s;
    }
}
