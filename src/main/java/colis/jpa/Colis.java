package colis.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Colis implements Serializable {

    /**
     * Identifiant du colis
     */
    @Id
    @GeneratedValue
    private long id;

    /**
     * Poids du colis(kg)
     */
    private float poids;

    /**
     * Valur du colis
     */
    private float valeur;

    /**
     * Lieu d'expédition du colis
     */
    private String origine;

    /**
     *  Lieu d'arrivée du colis
     */
    private String destination;

    @OneToMany(targetEntity=Suivi.class, mappedBy = "colis", fetch = FetchType.EAGER)
    private List<Suivi> suivis = new ArrayList<>();

    public Colis() {
    }

    public Colis(float poids, float valeur, String origine, String destination) {
        this.poids = poids;
        this.valeur = valeur;
        this.origine = origine;
        this.destination = destination;
    }

    public long getId() {
        return id;
    }

    public float getPoids() {
        return poids;
    }

    public float getValeur() {
        return valeur;
    }

    public String getOrigine() {
        return origine;
    }

    public String getDestination() {
        return destination;
    }

    public List<Suivi> getSuivis() {
        return suivis;
    }

    public void addSuivi(Suivi s) {
        this.suivis.add(s);
    }
}
