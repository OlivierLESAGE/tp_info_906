package colis.jpa;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Suivi implements Serializable {
    
    @Id
    @GeneratedValue
    private long id;

    private double latitude;
     
    private double longitude;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dateEntree;

    private String emplacement;

    private String etat;

    @ManyToOne @JoinColumn(nullable=false)
    private Colis colis;

    public Suivi(){}

    public Suivi(Colis colis ,double latitude, double longitude, Date dateEntree, String emplacement, String etat) {
        this.colis = colis;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateEntree = dateEntree;
        this.emplacement = emplacement;
        this.etat = etat;
    }

    public long getId() {
        return id;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public Date getDateEntree() {
        return dateEntree;
    }

    public String getEmplacement() {
        return emplacement;
    }

    public String getEtat() {
        return etat;
    }

    public Colis getColis() {
        return colis;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public void setDateEntree(Date dateEntree) {
        this.dateEntree = dateEntree;
    }

    public void setEmplacement(String emplacement) {
        this.emplacement = emplacement;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
}
