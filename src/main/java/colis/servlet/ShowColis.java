package colis.servlet;

import colis.ejb.ColisEJB;
import colis.jpa.Colis;
import colis.jpa.Suivi;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/ShowColis")
public class ShowColis extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // injection de la reference de l'ejb
    @EJB
    private ColisEJB ejb;

    public ShowColis() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        long id = -1;
        if (!req.getParameter("id").isEmpty()){
            id = Long.parseLong(req.getParameter("id"));
        }
        Colis c = ejb.getColis(id);
        req.setAttribute("colis", c);
        req.getRequestDispatcher("/showColis.jsp").forward(req, resp);
    }

}