package colis.servlet;

import colis.ejb.ColisEJB;
import colis.jpa.Suivi;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AddSuivi")
public class AddSuivi extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // injection de la reference de l'ejb
    @EJB
    private ColisEJB ejb;

    public AddSuivi() {
        super();
    }


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/addSuivi.jsp").forward(req, resp);
    }


    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        long id = -1;
        if(!req.getParameter("id").isEmpty()){
            id = Long.parseLong(req.getParameter("id"));
        }

        Double latitude = 0.0;
        if (!req.getParameter("latitude").isEmpty()) {
            latitude = Double.parseDouble(req.getParameter("latitude"));
        }

        Double longitude = 0.0;
        if (!req.getParameter("longitude").isEmpty()){
            longitude = Double.parseDouble(req.getParameter("longitude"));
        }

        String emplacement = req.getParameter("emplacement");
        String etat = req.getParameter("etat");

        Suivi s = ejb.addSuivi(id , latitude, longitude, emplacement, etat);

        resp.sendRedirect(req.getContextPath() + "/ShowColis?id=" + id);
    }
}