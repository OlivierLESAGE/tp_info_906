package colis.servlet;

import colis.ejb.ColisEJB;
import colis.jpa.Colis;
import colis.jpa.Suivi;

import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/AddColis")
public class AddColis extends HttpServlet {
    private static final long serialVersionUID = 1L;

    // injection de la reference de l'ejb
    @EJB
    private ColisEJB ejb;

    public AddColis() {
        super();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.getRequestDispatcher("/addColis.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        float poids = 0.0f;
        if (!req.getParameter("poids").isEmpty()) {
            poids = Float.parseFloat(req.getParameter("poids"));
        }

        float valeur = 0.0f;
        if(!req.getParameter("valeur").isEmpty()){
            valeur = Float.parseFloat(req.getParameter("valeur"));
        }
        
        String origine = req.getParameter("origine");
        String destination = req.getParameter("destination");

        Colis c = ejb.addColis(poids, valeur, origine, destination);

        resp.sendRedirect(req.getContextPath() + "/ShowColis?id=" + c.getId());
    }
}