<%@ page import="colis.jpa.Colis" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: Olivi
  Date: 22/09/2020
  Time: 11:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Affichage d'un colis</title>
    <!--<link href="default.css" rel="stylesheet" type="text/css" >-->
</head>
<body>
    <div class="container">
        <div class="card">
            <div class="card-header">
                <a href="index.jsp" class="btn btn-primary">revenir à la page d'accueil</a>
            </div>
            <div class="card-body">
    
        <c:choose>
            <c:when test="${not empty colis}">
                <h1>Colis</h1>
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">N°</th>
                            <th scope="col">Poids</th>
                            <th scope="col">Valeur</th>
                            <th scope="col">Origine</th>
                            <th scope="col">Destination</th>
                        </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">
                                ${colis.id}
                        </th>
                        <td>
                                ${colis.poids}kg
                        </td>
                        <td>
                                ${colis.valeur}€
                        </td>
                        <td>
                                ${colis.origine}
                        </td>
                        <td>
                                ${colis.destination}
                        </td>
                    </tr>
                    </tbody>
                </table>
        <c:choose>
            <c:when test="${not empty colis.suivis}">
                <table class="table">
                    <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Latitude</th>
                        <th scope="col">Longitude</th>
                        <th scope="col">Emplacement</th>
                        <th scope="col">Etat</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${colis.suivis}" var="suivi">

                        <form method="post" action="ShowColisModification">
                            <tr>
                                <td>
                                        ${suivi.dateEntree}
                                    <input id="id" name="id" type="hidden" value="${suivi.id}">
                                </td>
                                <td>
                                    <input name="latitude" type="number" step="0.01" value="${suivi.latitude}">
                                </td>
                                <td>
                                    <input name="longitude" type="number" step="0.01" value="${suivi.longitude }">
                                </td>
                                <td>
                                    <input name="emplacement" type="text" value="${suivi.emplacement }">

                                </td>
                                <td>
                                    <input name="etat" type="text" value="${suivi.etat }">
                                </td>
                                <td>
                                    <input type="submit" class="btn btn-primary " value="Modifier">

                                </td>
                            </tr>
                        </form>

                    </c:forEach>
                </tbody>
                </table>
            </c:when>

            <c:otherwise>
                <div class="alert alert-danger" role="alert">
                    Il n'y a pas de suivi pour ce colis
                </div>
            </c:otherwise>
        </c:choose>

    </c:when>
    <c:otherwise>
        <div class="alert alert-danger" role="alert">
            Ce colis n'existe pas !
        </div>

    </c:otherwise>
</c:choose>
</div>
</div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
<body>
</html>