<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>Ajout de colis</title>
</head>
<body>
<div class="container">
    <div class="card">
        <ul class="list-group">
            <li class="list-group-item">
            <h2>Ajouter une étape au suivi d'un colis : </h2>
            <form method="post" action="AddSuivi">
                Numéro de colis: <input name="id"class="form-control" type="number" value=""> <br/>
                Latitude : <input name="latitude" class="form-control" type="number" step="0.01" value=""><br>
                Longitude : <input name="longitude" class="form-control" type="number" step="0.01" value=""> <br/>
                Emplacement : <input name="emplacement" class="form-control" type="text" value=""> <br/>
                Etat : <input name="etat" class="form-control" type="text" value=""> <br/>
                <input type="submit" class="btn btn-primary float-right" value="OK">
            </form>
        </li>
        <li class="list-group-item">
            <h2>Modifier un colis : </h2>
            <form method="get" action="ShowColisModification">
                Numéro de colis: <input name="id" class="form-control" type="number" value=""> <br/>
                <input type="submit" class="btn btn-primary float-right"value="OK">
            </form>
            </li>
        </ul>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
</body>
</html>