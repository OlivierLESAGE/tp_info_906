# TP Info 906 - BROZZONI Vincent & LESAGE Olivier

## Utilisation
Nous avons créer le projet avec Gradle et utilisé WildFly pour le serveur.

## Fonctionalités
Lorsque vous arrivez sur la page d'accueil, vous avez la possibilité d'afficher un colis en saissant son numéro. 
    - En cliquant sur le bouton "Ajouter un colis" vous aurez accés au formulaire permettant de créer un nouveau colis. 
    - En cliquant sur le bouton "Ajouter un suivi", vous aurez la possibilité de créer une nouvelle étape de suivi pour un colis ou modifier les étapes de suivi d'un colis.
    
## Structure
### EJB
Nous avons un seul EJB appelé "ColisEJB", il permet la gestion des colis ainsi que leur suivis.

### JPA
Nous avons créer deux `Entity`. La première "Colis" représente les colis. La deuxième "Suivi" représente une étape de suivi rattaché à un colis.

### Servlet
Nous avons 4 `Servlet`:
* AddColis:
  * GET: Affiche le formulaire permettant de saisir les informations d'un colis.
  * POST: Récupère les informations du formulaire pour créer le colis dans la base de donnée puis redirige vers l'affichage des informations du colis.
* AddSuivi:
  * GET: Affiche le formulaire permettant de saisir les informations d'un suivi.
  * POST: Récupère les informations du formulaire pour créer une étape de suivi dans la base de donnée puis redirige vers l'affichage des informations du colis.
* ShowColis:
  * GET: Récupère un colis avec son numéro puis redirige vers l'affichage des informations du colis.
* ShowColisModification:
  * GET: Récupère un colis avec son numéro puis redirige vers la page premettant de modifier les étapes de suivi d'un colis.
  * POST: Récupère les informations modifié d'une étape de suivi de colis pour les mettre à jour dans la base de données puis redirige vers l'affichage des informations du colis.

### Contribution
BROZZONI Vincent & LESAGE Olivier

